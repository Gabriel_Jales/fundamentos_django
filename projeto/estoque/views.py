from django.shortcuts import render
from django.http import HttpResponse
from .models import Produto
import datetime

def home(request):
    data = {}
    data['now'] = datetime.datetime.now()
    return render(request, 'estoque/home.html', data)

def listagem(request):
    data = {}
    data['produtos'] = Produto.objects.all()
    return render(request, 'estoque/listagem.html', data)