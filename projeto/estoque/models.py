from django.db import models

class Categoria(models.Model):
    nome = models.CharField(max_length = 100)
    data_criacao = models.DateTimeField()

    class Meta:
        verbose_name_plural = 'Categorias'

    def __str__(self):
        return self.nome

class Produto(models.Model):
    nome = models.CharField(max_length = 100)
    descricao = models.CharField(max_length = 200)
    valor_uni = models.DecimalField(max_digits=7, decimal_places=2)
    valor_atacado = models.DecimalField(max_digits=7, decimal_places=2)
    estoque = models.IntegerField()
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Produtos'

    def __str__(self):
        return self.nome